apsfilter (7.2.6-5) unstable; urgency=medium

  * QA upload.

  * Corrected Vcs links in d/control.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 15 Nov 2024 15:08:09 +0100

apsfilter (7.2.6-4) unstable; urgency=medium

  * QA upload.

  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.6.2 to 4.7.0.
  * Use wrap-and-sort -at for debian control files.
  * Updated vcs in d/control to Salsa.

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 14 Nov 2024 07:24:52 +0100

apsfilter (7.2.6-3) unstable; urgency=medium

  * QA upload
  * Packaging cleanup. Closes: #965410, #999010

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sat, 24 Dec 2022 12:08:28 +0100

apsfilter (7.2.6-2) unstable; urgency=medium

  * QA upload.
  * Change maintainer to QA group.
  * d/control: Change Priority to optional.
  * Explicit pass `--with-shell=/bin/bash` to configure to make build
    reproducible between merged-usr and non-merged-usr systems.
    (Closes: #915166)

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 01 Dec 2018 16:28:30 +0100

apsfilter (7.2.6-1.3) unstable; urgency=low

  * Non-maintainer upload.
  * bin/setup_dvips.sh: Fix bashism. (Closes: #489536)
  * Don't ignore errors by make
  * Set compat level to five.
  * Bump debhelper dep and move it to b-d.
  * Add a dependency on misc:depends.
  * Suggest ghostscript instead of gs.

 -- Raphael Geissert <geissert@debian.org>  Thu, 02 Jul 2009 20:50:56 -0500

apsfilter (7.2.6-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/rules: Fix bashim. (Closes: #459048)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Mon, 28 Jan 2008 00:42:50 +0100

apsfilter (7.2.6-1.1) unstable; urgency=low

  * Non-maintainer upload to fix longstanding l10n and debconf issues
  * Remove the debconf note which deals with a pre-sarge version
    That implies removing debian/config, debian/templates and debian/po/
    (closes: #377973)
  * Depending on debconf is not needed anymore (closes: #331744)
  * Debconf translations not needed anymore (closes: #313111, #315815, #341555)

 -- Christian Perrier <bubulle@debian.org>  Thu, 26 Oct 2006 18:41:25 +0200

apsfilter (7.2.6-1) unstable; urgency=low

  * New upstream version
  * Fixed sendmail check in configure to produce warning instead of error
    (closes: #269505)

 -- Pawel Wiecek <coven@debian.org>  Tue, 15 Feb 2005 23:16:47 +0100

apsfilter (7.2.5-5) unstable; urgency=low

  * Added japanese debconf templates from Hideki Yamane <henrich@samba.gr.jp>
    (closes: #236818)
  * While at that -- fixed some grammar errors in polish translations, I have
    no idea where they could possibly come from
  * Removed recommendation of (a2ps | mpage | recode | enscript) and added a
    dependency on a2ps instead (closes: #250163)
  * Added danish debconf templates from Michael Kristensen <michael@wtf.dk>
    (closes: #253055)
  * Replaced build-dependency on MTA with --with-sendmail option to configure
    (no fcking idea why I didn't do it in 7.2.5-2) (closes: #262236)
  * Added support for gs 8.xx as an ugly hack. Serious support should be made,
    but this seems more like important upstream change (closes: #267258)

 -- Pawel Wiecek <coven@debian.org>  Tue, 31 Aug 2004 23:38:13 +0200

apsfilter (7.2.5-4) unstable; urgency=low

  * Fixed config dir location inside apsfilter binary (closes: #230306,
    #232104)

 -- Pawel Wiecek <coven@debian.org>  Thu, 12 Feb 2004 13:44:57 +0100

apsfilter (7.2.5-3) unstable; urgency=low

  * This release closes mostly bugs that were easy to fix. More is on the way.
  * Applied a set of patches from Martin Pitt <martin@piware.de> to fix
    multiple bugs (closes: #146724, #190169, #216438, #201273, #201274,
    #201288, #201291)
  * Applied a patch from <ibaldo@adinet.com.uy> to workaround a bug in nprint
    (closes: #203217)
  * Switched to gettext-based debconf templates and added french translations
    from Christian Perrier <bubulle@debian.org> (closes: #205802, #206435)
  * Added patch from Marc Wilson <msw@cox.net> to fix manual input tray
    support (closes: #227245)
  * Marked STP drivers as obsolete and added suggestion that ijsgimpprint
    should be used (closes: #170827)
  * Changed permissions on /etc/apsfilter/QUEUE/netware.conf as set by
    apsfiltersetup (closes: #203216)

 -- Pawel Wiecek <coven@debian.org>  Wed, 28 Jan 2004 14:19:35 +0100

apsfilter (7.2.5-2) unstable; urgency=low

  * Added exim|mail-transport-agent to build dependencies (closes: #187643)
  * Updated standards version
  * Install correct manpages (closes: #144934)
  * More fixes to come
  * Removed dependency on awk (suggested by lintian)

 -- Pawel Wiecek <coven@debian.org>  Fri, 25 Apr 2003 15:08:28 +0200

apsfilter (7.2.5-1) unstable; urgency=low

  * New upstream version

 -- Pawel Wiecek <coven@debian.org>  Tue, 26 Nov 2002 13:48:06 +0100

apsfilter (7.2.3-1) unstable; urgency=low

  * New upstream version

 -- Pawel Wiecek <coven@debian.org>  Mon, 28 Oct 2002 09:58:08 +0100

apsfilter (7.2.2-3) unstable; urgency=low

  * Moved aps2file, apspreview and apsfilter-bug to /usr/bin (closes: #138580)

 -- Pawel Wiecek <coven@debian.org>  Mon, 18 Mar 2002 21:46:11 +0100

apsfilter (7.2.2-2) unstable; urgency=low

  * Added russian translation of debconf templates (closes: #137615)
  * Now installs drivers directory as it should (closes: #138081)
  * Now uses /usr/bin/awk instead of what gets autodetected at build time
    (closes: #138085)

 -- Pawel Wiecek <coven@debian.org>  Wed, 13 Mar 2002 13:35:28 +0100

apsfilter (7.2.2-1) unstable; urgency=low

  * New upstream version (closes: #127039)
  * Now correctly specifies shell (closes: #121257)
  * Uses mpage, recode and enscript as alternatives to a2ps (closes: #103129)
  * There are probably more bugs I can close with this release -- I'd have to
    test them more extensively first

 -- Pawel Wiecek <coven@debian.org>  Thu, 28 Feb 2002 14:46:57 +0100

apsfilter (6.1.1-6) unstable; urgency=low

  * Added brazilian portuguese translations of debconf templates
    (closes: #107032)
  * Updated standards version

 -- Pawel Wiecek <coven@debian.org>  Wed,  1 Aug 2001 16:07:19 +0200

apsfilter (6.1.1-5) unstable; urgency=low

  * Added spanish translations of debconf templates (closes: #103234).

 -- Pawel Wiecek <coven@debian.org>  Tue,  3 Jul 2001 11:34:29 +0200

apsfilter (6.1.1-4) unstable; urgency=low

  * Added polish translations of debconf templates.

 -- Pawel Wiecek <coven@debian.org>  Tue, 22 May 2001 13:08:49 +0200

apsfilter (6.1.1-3) unstable; urgency=low

  * Forgot to set maintainer field last time :^) Now doing this.
    (closes: #87006)

 -- Pawel Wiecek <coven@debian.org>  Sat, 12 May 2001 00:34:29 +0200

apsfilter (6.1.1-2) unstable; urgency=low

  * New maintainer (closes: #87006)

 -- Pawel Wiecek <coven@debian.org>  Fri, 11 May 2001 09:59:14 +0200

apsfilter (6.1.1-1) unstable; urgency=low

  * New upstream version.
  * Moved to debhelper 3.
  * Added template translations to Swedish (by Andre Dahlqvist; closes:
    #83539), Dutch (by Thomas J. Zeeman; closes: #83904), and German (by
    Leonard Michlmayr; closes: #83957, #84258).
  * debian/rules: Include contributed .upp profiles.  Closes: #95680.
  * Removed pbm2ppa from Suggests since it is no longer in the archive.
  * Conforms to Standards version 3.5.4.

 -- Matej Vela <vela@debian.org>  Wed,  2 May 2001 07:16:13 +0200

apsfilter (6.0.0-1) unstable; urgency=low

  * New upstream version.  Closes: Bug#65831, Bug#70955, Bug#71147.
    * Configuration incompatible with previous versions; a warning is
      displayed on upgrade (using debconf) and old configuration files
      are backed up.
    * Closer to FHS compliance; all changes needed for full compliance
      have been made transparent using symbolic links.  README.Debian no
      longer necessary.
    * debian/apsfilterconfig.8: Obsoleted by man/apsfilter.1.
  * Orphaning.
  * Handle systems where there is no /etc/printcap.  Closes: Bug#70943.
  * /etc/apsfilter is not included directly in the package to avoid
    spurious `directory not empty so not removed' warnings from dpkg.
  * debian/postinst: Don't call apsfilterconfig since it doesn't use
    debconf yet.
  * debian/rules: Install only the files installed by the upstream
    Makefile.
  * Conforms to Standards version 3.2.1.

 -- Matej Vela <vela@debian.org>  Wed,  3 Jan 2001 16:06:40 +0100

apsfilter (5.1.4-1) unstable; urgency=low

  * New upstream version.  Unfortunately, the forwarded Debian patches
    have not been included yet.
  * debian/docs: Added doc/REMOTE-PRINTING.
  * Conforms to Standards version 3.1.1.

 -- Matej Vela <vela@debian.org>  Thu, 13 Jan 2000 17:31:22 +0100

apsfilter (5.1.3-4) unstable; urgency=low

  * Conforms to Standards version 3.1.0:
    * Installing the /usr/doc symbolic links.
    * debian/control: Added build dependancies.

 -- Matej Vela <vela@debian.org>  Sun,  7 Nov 1999 09:21:38 +0100

apsfilter (5.1.3-3) unstable; urgency=low

  * setup/filtersetup: tempfile(1) from debianutils (>= 1.12) creates
    files with 0600 modes, so an additional chmod is neccessary.
    Closes: Bug#46051.

 -- Matej Vela <vela@debian.org>  Mon, 27 Sep 1999 05:12:14 +0200

apsfilter (5.1.3-2) unstable; urgency=low

  * Installing INSTALL to /usr/doc/apsfilter because it explains
    printing at different resolutions.  Closes: Bug#44676.
  * Wrote debian/README.Debian.
  * SETUP: Removed the note about where to find Uli Wortmann's HP
    drivers since they are included in gs (>= 5.10-2).

 -- Matej Vela <vela@debian.org>  Sun, 12 Sep 1999 15:17:42 +0200

apsfilter (5.1.3-1) unstable; urgency=high

  * New upstream version.  Closes: Bug#43695.
  * Disabled the LPRng bounce queues patch since it was causing
    problems for some users.  I'll let upstream decide how it should
    be implemented.  Closes: Bug#42157.
  * Cleaned up Debian-specific patches.
  * debian/copyright: Updated upstream URL.
  * Conforms to Standards version 3.0.1:
    * Changed debian/copyright to reference
      `/usr/share/common-licenses/GPL'.

 -- Matej Vela <vela@debian.org>  Wed,  1 Sep 1999 21:08:37 +0200

apsfilter (5.1.2-2) unstable; urgency=low

  * Included patch for handling LPRng bounce queues from
    Carsten Paeth <calle@calle.in-berlin.de>.
  * Made SETUP more robust when /var/lib/apsfilter doesn't exist.
    Closes: Bug#39739.
  * SETUP: spelling corrections, small bug fixes.

 -- Matej Vela <vela@debian.org>  Sun, 27 Jun 1999 16:35:32 +0200

apsfilter (5.1.2-1) unstable; urgency=low

  * New upstream version.
  * Changed the base directory to /usr/share/apsfilter to comply with
    the soon-to-be-adopted FHS.

 -- Matej Vela <vela@debian.org>  Sun, 13 Jun 1999 16:52:42 +0200

apsfilter (5.1.1-2) unstable; urgency=low

  * Added support for Martin Lottermoser's hpdj Ghostscript driver.
    Closes: Bug#38525.
  * Removed doc/readme.hp8 from debian/docs.

 -- Matej Vela <vela@debian.org>  Sun, 30 May 1999 13:15:33 +0200

apsfilter (5.1.1-1) unstable; urgency=low

  * New upstream version.
  * bin/apsfilter: LPRng uses `(STDIN)' as job name when standard
    input is being processed.
  * Changed dependency on lpr to `lpr | lprng', to make it clear that
    LPRng is supported.
  * Updated the list of supported formats in the description.
  * Changed Priority to extra since this package conflicts with
    magicfilter.
  * Conforms to Standards version 2.5.1.

 -- Matej Vela <vela@debian.org>  Sat, 29 May 1999 18:10:16 +0200

apsfilter (5.0.1-2) unstable; urgency=low

  * bin/apsfilter: Fixed a big typo.
  * bin/apsfilter: Print errors on stderr instead of /dev/console.
    (This way they'll be properly logged.)

 -- Matej Vela <vela@debian.org>  Mon, 19 Apr 1999 09:55:43 +0200

apsfilter (5.0.1-1) unstable; urgency=low

  * New upstream version.
  * New maintainer; packaging done from scratch.  Old patches
    reapplied with some minor improvements.
  * Added Depends: awk (used in bin/apsfilter).
  * Changed Suggests: xfig to Suggests: transfig since fig2dev is
    located in the latter.
  * Changed everything to comply with the FSSTND by using
    /var/lib/apsfilter for dynamic data (i.e. filter symlinks).
  * bin/apsfilter: Adapted to use GNU a2ps instead of AKL's a2ps,
    since the latter is only an obsolete incarnation of the former.
    Added a2ps to Recommends.
  * rewindstdin has been obsoleted, and AKL's a2ps isn't being used
    anymore, so this package no longer contains binaries.  Changed
    Architecture to all.
  * debian/postinst is now merely a wrapper around SETUP.  SETUP is
    installed as apsfilterconfig(8).  Closes: Bug#28666.
  * Wrote a manual page for apsfilterconfig(8).
  * bin/setup_dvips.sh: Added a Debian-specific default value for
    CONFIG_DIR (/etc/texmf/dvips) to bin/setup_dvips.sh.
  * global/GLOBAL.sh: Added a shebang.
  * SETUP: Removed calls to do_check_permissions and
    do_cleanup_for_linux since they aren't applicable to Debian.
    Removed call to do_finish_setup since that function doesn't
    exist.  Added some cosmetic calls to echo.
  * SETUP: Uses paperconf(1) if available to find out the default
    paper format.
  * SETUP: Added function do_show_devices which looks for
    devices.txt.gz in /usr/doc/{gs,gs-alladin}/.  (setup/devices.txt
    is no longer included in the package.)
  * SETUP: References the appropriate HOWTO's.  Made example device
    names Linux-specific.  Removed FreeBSD specifics from messages.

 -- Matej Vela <vela@debian.org>  Sat, 17 Apr 1999 19:01:59 +0200

apsfilter (4.9.7-5) unstable; urgency=low, closes=28729

  * The postinst now works even when no /etc/printcap is present.  Thanks
    to Franz Schwarz <fschwarz@gmx.net> and Landon Fuller
    <swift@mercury.warped-reality.com> (closes: Bug#28729)
  * Fixed a thinko in filterconfig

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Fri, 30 Oct 1998 20:33:12 +0100

apsfilter (4.9.7-4) unstable; urgency=low, closes=28259

  * chmod 0644 of filters_found, one missing.  Thanks to Franz Schwarz
    <fschwarz@gmx.net> (closes: Bug#28259)
  * Changed one misleading 'test -s' into 'test -d'.  Thanks for the
    inspiration also go to Franz Schwarz <fschwarz@gmx.net>.

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Wed, 28 Oct 1998 10:32:08 +0100

apsfilter (4.9.7-3) unstable; urgency=low

  * chmod 0644 of filters_found, caused a problem when installed with
    different umask than 022

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sat, 17 Oct 1998 20:49:09 +0200

apsfilter (4.9.7-2) unstable; urgency=low, closes=23593

  * Modified apsfilter and setup_dvips to make use of mktemp (closes: Bug#23593)

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Fri, 16 Oct 1998 18:03:46 +0200

apsfilter (4.9.7-1) unstable; urgency=low

  * New upstream release
  * Modified a2ps.c so -Wall doesn't spread out warnings
  * ite'ed postinst question concerning papersized
  * Relaxed syntax for postinst question concerning device

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Tue, 30 Jun 1998 02:16:07 +0200

apsfilter (4.9.1-12) unstable; urgency=low, closes=23147

  * Fixed permission problem (closes: Bug#23147)

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Tue, 9 Jun 1998 04:19:34 +0200

apsfilter (4.9.1-11) frozen unstable; urgency=low, closes=7175 9454 3812 4948 8137 12100 12398 13046 13818 11542 12595 18764 21328 12397 8976 8992 12262 13822

  * New maintainer Joey
  * New packaging scheme (closes: Bug#9454)
  * Added my own debian/rules file
  * New description, new control file
  * New copyright file
  * Changed architecture to any as it contains two compiled C programs
    (closes: Bug#7175)
  * New .orig.tar.gz, removed binary file from it
  * Modified a2ps.c so -Wall doesn't spread out warnings
  * Re-included escp printer links, used hardlinks instead of multiple
    files
  * Removed setup/compile
  * Removed unsecure use of /tmp in postinst
  * Added dependency on file (closes: Bug#3812, Bug#8137)
  * Added suggestion for mailx (closes: Bug#4948)
  * Modified handling of zless and PAGER (closes: Bug#12100, Bug#12398,
    Bug#13046, Bug#13818, Bug#11542h)
  * Removed bell from postinst
  * Removed banner and more from postinst (closes: Bug#12595)
  * Made postrm recognize "remove"
  * Removed annoying clear calls from postinst
  * Removed 'set -x' from mksymlinks
  * Corrected apsfilter to print jpeg images, thanks to Andreas Neubacher
    <andreas.neubacher@acm.org> for investigating this (closes: Bug#18764)
  * Renamed manpage into apsfilter-a2ps.1 (closes: Bug#21328, Bug#8976,
    Bug#8992)
  * Corrected name of GIF filter, thanks to Florian Hars
    <hars@math.uni-hamburg.de> for investigating this (closes: Bug#12397)
  * Added Suggests for netpbm
  * Moved /etc/printcap into /usr/doc/apsfilter (closes: Bug#12262)
  * Added configuration for Linux sytems to GLOBAL.sh, this includes
    /var/spool instead of /usr/spool (closes: Bug#13822)
  * Hardcoded library directory as it's not guaranteed to be noted in
    /etc/printcap.
  * Removed several sleep calls from postinst

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sat, 30 May 1998 18:08:04 +0200

apsfilter (4.9.1-11) frozen unstable; urgency=low, closes=7175 9454


 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sat, 30 May 1998 13:58:12 +0200
