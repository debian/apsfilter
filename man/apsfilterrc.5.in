.\" Copyright (c) 2000-2002
.\"     Andreas Klemm <andreas@apsfilter.org>.  All rights reserved.
.\"
.\" $ApsCVS: src/apsfilter/man/apsfilterrc.5.in,v 1.10.2.6 2002/11/10 16:08:53 andreas Exp $
.\"                                                                           
.Dd April 08, 2000
.Dt APSFILTERRC 5
.Os
.Sh NAME
.Nm apsfilterrc
.Nd configuration file for magic print filter
.Sh SYNOPSIS
.Nm apsfillterrc
.Sh DESCRIPTION
.Nm Apsfilterrc
is a configuration file for printers controlled by the
.Xr apsfilter 1
magic filter.
.Pp
Every apsfilter printer in
.Xr printcap 5
has an
.Nm
file of its own.
.Pp
There is one global apsfilterrc file that controls print
characteristics for all printer. The printer-specific
.Nm
files have preference over the global one.
.Pp
Reasons for using an
.Nm
file are if it turns out, that you have to type in some commandline
options too often or if an application is unable to parameterize
the print command, so that you have to hardcode the print characteristic
for such dumb applications.
.Pp
An enhancement to this concept are user-specific
config files, that have preference over the global and
the printer-specific config file. This allows configuration
of print characteristics on a per-user basis.
.Pp
You should make use of user-specific config files instead
of using the deprecated USE_USER_CODE variable, which
compromises system security and is only an option on
a single-user machine, see below.
.Ss Capabilities
.Bl -tag -width THIS_MUCH_SPACE
.It Nm QUALITY
Select print quality (draft, lo|low, med|medium, hi|high, photo).
.It Nm SWEEP
Print uni- or bidirectional (uni, bi).
.It Nm MEDIA
Select paper type (plain, coated, glossy, premium, trans).
.It Nm COLOR
Select color mode (color|colour, gray|grey, mono).
.It Nm METHOD
Choose apsfilters method of data processing (auto, ascii, raw).
Automatic file type recognition and automatic conversion to Postscript
is apsfilters default processing method. If you want to print mostly
files `as is', you can set the default to 'raw', this disables any kind
of file type recognition and conversion. If you want to print mostly
everything as ASCII text, then you can set this option to 'ascii'.
.It Nm PAPERSIZE
Set the default paper size (a3, a4, legal, ledger, tabloid).
.It Nm ASCII_FILTER
Text file filter to use, when printing ASCII documents
(a2ps, mpage, enscript, recode).
.It Nm PAPERTRAY
Paper feed tray number (tray0...tray9).
.It Nm PRETTY_PRINTING
a2ps highlight level for pretty-printing when using a2ps or enscript
as ascii filter (0, 1, 2). See also ASCII_FILTER.
.It Nm ASCII_HEADER
Whether you want headers in your text prints when using a2ps, mpage
or enscript as ascii filter (header, noheader).
.It Nm ASCII_BORDER
Whether you want borders in your text prints  when using a2ps, mpage
or enscript as ascii filter (border, noborder).
.It Nm PS_NUP|ASCII_PPS
Pages per sheet (1pps, 2pps, 4pps, 8pps).
.It Nm LANDSCAPE|ASCII_LANDSCAPE
Paper orientation (landscape, portrait).
.It Nm PS_BOOK
Output pages in "book" format, implies commandline options
"2pps,duplex,shortbind". Related options: PS_NUP, ASCII_PPS,
DUPLEX, BINDING.
.It Nm DUPLEX
Whether to use duplex mode or not (duplex, simplex).
.It Nm BINDING
Paper binding edge (shortbind, longbind).
.It Nm COPIES
Number of copies (copies=N).
.It Nm USE_USER_CODE
Enabling this option is a big potential security hole !!!
If you don't understand this, DO NOT USE IT.
It makes user manageable
.Nm
files possible which will be sourced (included/executed)
during runtime of a printjob under the permissions of the
calling process (lpd).
This option is only valid for typical "single-user" systems,
where a user wants direct access to an apsfilterrc without
having to become root to make changes.
A more conveniant way on a multi-user system, where security
is an issue, is that a system administrator creates user-specific
.Nm
files. Additional informations can be found in the apsfilter
handbook and in the apsfilterrc file together with some examples.
.Sh FILES
.Bl -tag -width THIS_MUCH_SPACE -compact
.It Pa @sysconfdir@/apsfilter/apsfilterrc
apsfilter
.Em global
config file, settings are overwriteable by more specific
printer config files, see below.
.It Pa @sysconfdir@/apsfilter/QUEUE/apsfilterrc
apsfilter config file on a
.Em per printer queue
basis. This file has preference over the global config file.
.It Pa @sysconfdir@/apsfilter/QUEUE/apsfilterrc.USER
apsfilter config file on a
.Em per user
basis (per print queue). This file has preference over the global
and the printer queue specific config file.
.It Pa $HOME/.apsfilter/apsfilterrc.QUEUE
A 
.Em user defineable
config file on a per printer queue basis. This file has
preference over the global, the printer queue specific and the central
per user config file, which is administered by a system administrator.
Enabling this has a deep impact on your systems security,
.Em should NOT be used,
especially on multi-user systems and is therefore
.Em disabled by default.
See comments to USE_USER_CODE variable.
.Sh SEE ALSO
.Xr apsfilter 1 ,
.Xr printcap 5
.Sh BUGS
See apsfilter software center -
.Li http://www.apsfilter.org/
- for new versions, bugfixes and known bugs.
.Pp
Please use the new tool
.Xr apsfilter-bug 1
to release bug- or problem reports. It automatically presents you a
.Nm form
in an editor window which asks you some standard questions.
If you save and quit the editor session, then this report is
sent automatically via 
.Nm e-mail
to the proper apsfilter mailinglist.
.Pp
If apsfilter fails to print something or prints it in a way
you wouldn't expect and you want to report an apsfilter error
then please save the debugging output of one print session
using the new
.Xr aps2file 1
utility by typing
.Nm aps2file -D -Z options file > /dev/null 2> file.debug
and including the debugging output in the file
.Nm file.debug
into the edit session of the
.Nm apsfilter-bug
utility, so that it is included into the mail to the apsfilter
mailinglist.
.Pp
Please note that you need to run /bin/sh (Bourne Shell), bash
or a compatible shell, so that the above mentioned output
redirection works. Under C-shell (/bin/csh) or tcsh it would't
work. If you don't know, then simply make sure you use the
Bournce shell by typing
.Nm /bin/sh
or
.Nm bash ,
then you should have no problems with redirection of
.Nm stdout
and
.Nm stderr
(> /dev/null 2> file.debug).
.Sh DOCUMENTATION
See official apsfilter homepage
.Bl -tag -width XXXXXXXXXXXXXTHIS_MUCH_SPACEXXXXXXXXXXX -compact
.It Pa http://www.apsfilter.org/handbook.html
Apsfilter Handbook including the Frequently Asked Questions (FAQ)
.Sh USER FORUM
Please send questions to the official apsfilter help channel
.Ic apsfilter-help@apsfilter.org .
The above section
.Nm BUGS
and the file
.Ic HOWTO-BUGREPORTS
tells you how to report bugs.
If you want to know how to troubleshoot your apsfilter installation,
please read the manpage
.Xr aps2file 1
and
.Xr apsfilter-bug 1
as well as the
.Ic Apsfilter Handbook
carefully.
.Sh HISTORY
.Nm Apsfilter
developement started in 1994.
.Pp
The
.Nm
file on a "per printer queue" basis appeared in apsfilter 5.4.0.
