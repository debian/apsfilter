// Program translates .lpt file (printer font 6x8) to another encoding.

// $ApsCVS: src/apsfilter/contrib/ruslpdrv/transfont.c,v 1.2 2000/04/16 11:42:16 andreas Exp $

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STDIN	0
#define STDOUT	1
#define STDERR	2
#define CHARLEN 6
#define FALSE   0
#define TRUE    1

void CopyChar(unsigned char *CharTable,unsigned char *CharTableNew, int
CurrentChar, int NewPlace);
void EmptyArray(unsigned char *Array, int length);
void OpenReadFile(char *name,unsigned char *Array, int length);
void LoadTransTable(char *name, unsigned char *TransTable);
void Translate(unsigned char *CharTable, unsigned char *CharTableNew, unsigned char *TransTable);
void SaveFile(char *name, unsigned char *CharTableNew, int length);

void EmptyArray(unsigned char *Array, int length)
{
    int i;

    for(i=0;i<length;i++) Array[i]=0;  /* could use function memset */
}

void OpenReadFile(char *name,unsigned char *Array, int length)
{
    int handle;

    handle=open(name,O_RDONLY);
    if (handle==-1){
        perror(name);
        exit(1);
    }
    read(handle,Array,length);
    close(handle);
}

void LoadTransTable(char *name, unsigned char *TransTable)
{
    int i,what,where;
    char unsigned buffer[512];
    FILE *handle_f;
    char *readed_c;
    int isEndOfFile;

    for(i=0;i<256;i++) TransTable[i]=i; /* translation table */

// Opening translation file (and filling the array)
    if ((handle_f=fopen(name,"rt"))==NULL)
    {
        perror(name);
        exit(1);
    }
    
    isEndOfFile=FALSE;
    while(!isEndOfFile){
     	readed_c=fgets(buffer,512,handle_f);
        if ((readed_c==NULL) || (buffer[0]==13)){ isEndOfFile=TRUE;
continue; }
  	if (buffer[0]=='#') continue;
        sscanf(buffer,"%i%i",&what,&where);
        TransTable[where]=(unsigned char)what;
/*        TransTable[what]=(unsigned char)where;*/
    }
    fclose(handle_f);
}

void Translate(unsigned char *CharTable, unsigned char *CharTableNew,
unsigned char *TransTable)
{
    int CurrentChar;

    for(CurrentChar=32;CurrentChar<256;CurrentChar++)
        CopyChar(CharTable,CharTableNew,CurrentChar,TransTable[CurrentChar]);
}

void CopyChar(unsigned char *CharTable,unsigned char *CharTableNew, int CurrentChar, int NewPlace)
{
    int Octet;

    for(Octet=0;Octet<CHARLEN;Octet++)
        CharTableNew[NewPlace*CHARLEN+Octet]=CharTable[CurrentChar*CHARLEN+Octet];
}

void SaveFile(char *name, unsigned char *CharTableNew, int length)
{
    int FileHandle;

    FileHandle=open(name,O_WRONLY|O_CREAT,0664);
    write(FileHandle,CharTableNew,length);
    close(FileHandle);
}

void main(int argc, char *argv[]){
    unsigned char CharTable[256*CHARLEN];
    unsigned char CharTableNew[256*CHARLEN];
    unsigned char TransTable[256];

    EmptyArray(CharTable,256*CHARLEN);

    OpenReadFile("alt.lpt",&CharTable[32*CHARLEN],224*CHARLEN);

    LoadTransTable("alt2koi.tr",TransTable);

    Translate(CharTable,CharTableNew,TransTable);

    SaveFile("koi8-r.lpt",&CharTableNew[32*CHARLEN],224*CHARLEN);
}

